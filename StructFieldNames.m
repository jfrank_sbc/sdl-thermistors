fn = fieldnames(all_Ts_Data);
for k=1:numel(fn)
    if( isnumeric(all_Ts_Data.(fn{k})) )
        all_Ts_Data_f.(fn{k}) = downsample(all_Ts_Data.(fn{k}),4000);
    end
end