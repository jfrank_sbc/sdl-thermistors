%% Written by Jesse Frank
% 08/19/21

%%Takes data struct from SDL and converts raw SDL data to Skin Temp in DegC
%for each thermistor used.
%% Choose which file that has been loaded in to analyze.
% Based off of numbering system, with most recent test being the highest
% number. The first test saved since the SDL was last cleared of files is
% file 1.
%test_num = str2double(inputdlg('Which number test on the SDL device would you like to analyze?'));
%file_name = imported_files{1,test_num};
%data = SDL.importBinFile(file_name);

%% Convert data to skin Temp (Ts)
num_therm = str2double(inputdlg('How many thermistors were used for this test?'));

%Ts = []; %intialize variable

for j = 1 : num_therm
    a = calc_skin_temp(data,j);
    Ts(:,j) = a;
end

%% Filter data and plot
[y,x] = butter(2,0.1); 
outS = filtfilt(y,x,Ts);
figure;
plot(Ts,'o')
figure;
plot(outS,'o')
%legend('Hamstring','Lateral Knee','Quad','Medial Knee')
legend('A1','A2','A3')
figure;
Ts_d = downsample(Ts,400);
plot(Ts_d)
%%
function a = calc_skin_temp(d,j)

%% Constants
    ADC_Max = 8192;
    Rs = 10000; %Ohms
    B = 3960;
    Ro = 100000; %Ohms
    To = 298.15; %Kelvin
    Ts = [];


    for i = 1:length(d.A1_1)
        %Thermistor Resistance
        if j == 1
            Rth = Rs/((ADC_Max/d.A1_1(i))-1);
        elseif j == 2
            Rth = Rs/((ADC_Max/d.A2_1(i))-1);
        elseif j == 3
            Rth = Rs/((ADC_Max/d.A3_1(i))-1);
        elseif j == 4
            Rth = Rs/((ADC_Max/d.A4_1(i))-1);
        else
            error('SDL Thermistor capability is not currently setup for more than 4 thermistors. Please run this function again')
        end
        % Resistance to Temperature Kelvin
        Tk = 1/((1/To)+ ((1/B)*log(Rth/Ro)));

        %Kelvin to Celsius
        Tc = Tk -273.15;
        a(i,1) = Tc;
    end
end
    