figure;
subplot(3,1,1);
plot(c{2,1}(1:90,1))
hold on
plot(c{3,1}(1:90,1))
plot(c{4,1}(1:90,1))
plot(c{5,1}(1:90,1))
plot(c{6,1}(1:90,1))
legend('TLD 1','Kris Proto 1', 'Kris Proto 2','TLD 2','Nude','location','bestoutside')
title('Lateral Knee')
xlabel('Data Points (6 per minute)')
ylabel('Temp (degC)')
ylim([20 55])

subplot(3,1,2);
plot(c{2,1}(1:90,2))
hold on
plot(c{3,1}(1:90,2))
plot(c{4,1}(1:90,2))
plot(c{5,1}(1:90,2))
plot(c{6,1}(1:90,2))
legend('TLD 1','Kris Proto 1', 'Kris Proto 2','TLD 2','Nude','location','bestoutside')
title('Quad')
xlabel('Data Points (6 per minute)')
ylabel('Temp (degC)')

subplot(3,1,3);
plot(c{2,1}(1:90,3))
hold on
plot(c{3,1}(1:90,3))
plot(c{4,1}(1:90,3))
plot(c{5,1}(1:90,3))
plot(c{6,1}(1:90,3))
legend('TLD 1','Kris Proto 1', 'Kris Proto 2','TLD 2','Nude','location','bestoutside')
title('Medial Knee')
xlabel('Data Points (6 per minute)')
ylabel('Temp (degC)')